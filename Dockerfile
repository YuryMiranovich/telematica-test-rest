FROM openjdk:8-alpine
WORKDIR /app
COPY build/docker/libs libs/
COPY build/docker/resources resources/
COPY build/docker/classes classes/
ENV test.startdate 1597622400000
ENV test.rest.url http://195.93.181.7:7002
ENV auth.url http://195.93.181.7:7001/auth/login
ENTRYPOINT ["java", "-Xmx512m", "-cp", "/app/resources:/app/classes:/app/libs/*", "com.clevertec.restapi.test.RestApiTestApplication"]
EXPOSE 9007
