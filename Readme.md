gradlew dockerCreateDockerfile

docker build . -t testrest

docker run -d --name vtbTestRest -e cron.expr='0 0 2 ? * *' testrest

#Properties

- test.rest.url=http://195.93.181.7:7002
- auth.url=http://195.93.181.7:7001/auth/login
- auth.username=test
- auth.password=
- spring.datasource.url=jdbc:postgresql://195.93.181.7:5432/data
- spring.datasource.username=postgres
- spring.datasource.password=
- email.to=a.goncharov@avantern.ru
- email.from=miranovich98@gmail.com
- cron.expr=0 0 2 ? * *