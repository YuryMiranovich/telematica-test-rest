package com.clevertec.restapi.test.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ServiceResponse {
    @JsonProperty("Number")
    private String number;
    @JsonProperty("ApplicationNumber")
    private String applicationNumber;
    @JsonProperty("CurrentStatus")
    private String status;
    @JsonProperty("LastSyncDate")
    private Date lastSyncDat;
    @JsonProperty("LocationLinesID")
    private Long locationLinesId;
    @JsonProperty("AlarmDateTime")
    private Date alarmDateTime;
    @JsonProperty("Latitude")
    private Double latitude;
    @JsonProperty("Longitude")
    private Double longitude;
    @JsonProperty("BatteryCharge")
    private Double battery;
    @JsonProperty("FullAddress")
    private String fullAddress;
    @JsonProperty("Country")
    private String country;
    @JsonProperty("Region")
    private String region;
    @JsonProperty("Town")
    private String town;
    @JsonProperty("Street")
    private String street;
    @JsonProperty("House")
    private String house;
    @JsonProperty("numberRegion")
    private int numberRegion;
}
