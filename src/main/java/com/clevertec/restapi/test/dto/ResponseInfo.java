package com.clevertec.restapi.test.dto;

import com.clevertec.restapi.test.entity.Device;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Date;

@Data
@AllArgsConstructor
public class ResponseInfo {
    private Device device;
    private Date dateFrom;
    private Date dateTo;
    private String resp;
}
