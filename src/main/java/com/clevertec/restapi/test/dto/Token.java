package com.clevertec.restapi.test.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.Date;
import java.util.UUID;

@Data
public class Token {
    @JsonProperty("Token")
    private String value;
    @JsonProperty("Expired")
    private String expiredAt;
}

