package com.clevertec.restapi.test.service;

import com.clevertec.restapi.test.dto.RequestDto;
import com.clevertec.restapi.test.dto.ResponseInfo;
import com.clevertec.restapi.test.dto.ServiceResponse;
import com.clevertec.restapi.test.dto.Token;
import com.clevertec.restapi.test.entity.Device;
import com.clevertec.restapi.test.repository.DeviceRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDateTime;
import java.util.*;

@RequiredArgsConstructor
@Slf4j
@Component
public class RestTestService {
    @Value("${test.rest.url}")
    String testRestUrl;

    @Value("${test.startdate}")
    long startDate;

    @Value("${email.to}")
    String emailTo;
    @Value("${email.from}")
    String emailFrom;

    @Value("${auth.username}")
    String username;
    @Value("${auth.password}")
    String password;
    @Value("${auth.url}")
    String authUrl;

    private RestTemplate restTemplate = new RestTemplate();

    private final DeviceRepository deviceRepository;
    private final ObjectMapper objectMapper;
    private final JavaMailSender emailSender;

    static Map<Long, Date> lastDates = new HashMap<>();

    public void doWork() {
        // List<Device> deviceList = deviceRepository.findAllByCrtNameOrCrtName("avtt1", "avtt2");
        // deviceList = deviceList.stream().filter(d -> d.getLastUpdate() != null).collect(Collectors.toList());
        List<Device> deviceList = deviceRepository.findAll();
        log.info("DEVICE COUNT {}", deviceList.size());

        if (lastDates.isEmpty()) {
            for (Device device : deviceList) {
                lastDates.put(device.getId(), new Date(startDate));
            }
        }

        String token = getToken();

        List<ResponseInfo> notFoundResponses = new ArrayList<>();

        Date dateTo = new Date();

        for (Device device : deviceList) {
            Date dateFrom = lastDates.get(device.getId());
            String response = callServiceForDevice(token, device, dateFrom, dateTo);
            if (isNotFound(response)) {
                notFoundResponses.add(new ResponseInfo(device, dateFrom, dateTo, response));
            } else {
                try {
                    ServiceResponse serviceResponse = objectMapper.readValue(response, ServiceResponse[].class)[0];
                    lastDates.put(device.getId(), serviceResponse.getLastSyncDat());
                } catch (JsonProcessingException e) {
                    log.error("Error parsing response", e);
                }
            }
        }

        try {
            sendEmail(notFoundResponses);
        } catch (MessagingException | IOException e) {
            log.error("Error sending email", e);
        }
    }

    String getToken() {
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
            headers.setContentType(MediaType.APPLICATION_JSON);

            Map<String, String> creds = new HashMap<>();
            creds.put("UserName", username);
            creds.put("Password", password);

            HttpEntity<Map<String, String>> request = new HttpEntity<>(creds, headers);

            Token token = restTemplate.postForObject(authUrl, request, Token.class);
            log.info("Get token {}", token);
            return token.getValue();

        } catch (Exception e) {
            if (e instanceof HttpClientErrorException) {
                HttpClientErrorException ex = (HttpClientErrorException) e;
                log.error(ex.getLocalizedMessage());
                log.error(ex.getResponseBodyAsString());
            }
            throw new RuntimeException(e);
        }

    }

    String callServiceForDevice(String token, Device device, Date dateFrom, Date dateTo) {
        RequestDto requestDto = new RequestDto();
        requestDto.setNumbers(Collections.singletonList(device.getNumber()));
        requestDto.setDateFrom(dateFrom);
        requestDto.setDateTo(dateTo);

        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", "Bearer " + token);
        headers.set("Content-Type", "application/json");

        HttpEntity<RequestDto> request = new HttpEntity<>(requestDto, headers);
        String resp = restTemplate.postForObject(testRestUrl, request, String.class);

        log.info("Call service: {}. Response {}", requestDto, resp);

        return resp;
    }

    boolean isNotFound(String response) {
        return response.contains("No elements found");
    }

    void sendEmail(List<ResponseInfo> responses) throws MessagingException, IOException {
        String filename = ("not_found_vins_" + LocalDateTime.now())
                .replace('-', '_')
                .replace('.', '_')
                .replace(':', '_') + ".txt";
        log.info("Prepare email with file {}", filename);

        PrintWriter writer = new PrintWriter(new FileWriter(filename));
        writer.println("VIN;LAST_UPDATE;DATE_FROM;DATE_TO");
        for (ResponseInfo responseInfo : responses) {
            writer.println(responseInfo.getDevice().getNumber() + ";" +
                    responseInfo.getDevice().getLastUpdateTime() + ";" +
                    responseInfo.getDateFrom() + ";" +
                    responseInfo.getDateTo());
        }
        writer.close();


        MimeMessage message = emailSender.createMimeMessage();

        MimeMessageHelper helper = new MimeMessageHelper(message, true);

        helper.setFrom(emailFrom);
        helper.setTo(emailTo);
        helper.setText("");
        helper.setSubject("Not found VINs");

        FileSystemResource file
                = new FileSystemResource(new File(filename));
        helper.addAttachment(filename, file);

        emailSender.send(message);
        log.info("Email send with filename {}", filename);
    }

}
