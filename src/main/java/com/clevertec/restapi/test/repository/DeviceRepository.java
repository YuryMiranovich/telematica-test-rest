package com.clevertec.restapi.test.repository;

import com.clevertec.restapi.test.entity.Device;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DeviceRepository extends JpaRepository<Device, Integer> {
 //List<Device> findAllByCrtNameOrCrtName(String crtNameFirst, String crtNameSecond);
}
