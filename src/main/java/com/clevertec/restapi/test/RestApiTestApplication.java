package com.clevertec.restapi.test;

import com.clevertec.restapi.test.service.RestTestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

@SpringBootApplication
@EnableScheduling
public class RestApiTestApplication {

    @Autowired
    private RestTestService restTestService;

    public static void main(String[] args) {
        SpringApplication.run(RestApiTestApplication.class, args);
    }

    @Scheduled(cron = "${cron.expr}")
    public void send() {
        restTestService.doWork();
    }

}
