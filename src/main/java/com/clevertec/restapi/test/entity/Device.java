package com.clevertec.restapi.test.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;



@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "cars")
public class Device implements Serializable {
    @Id
    @JsonIgnore
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "name", length = 128, updatable = false)
    private String name;
    @Column(name = "number", length = 80, updatable = false)
    private String number;
    @Column(name = "application_number")
    private String applicationNumber;
    @Column(name = "uniqueid", length = 128, updatable = false, unique = true)
    private String imei;
    @Column(name = "last_update_time")
    private Date lastUpdateTime;
}
/*@Entity
@Data
@NoArgsConstructor
@Table(name = "tc_devices")
public class Device {
    @Id
    @Column(name = "id")
    private Integer id;
    @Column(name = "name")
    private String name;
    @Column(name = "uniqueid")
    private String uniqueId;
    @Column(name = "lastupdate")
    private Date lastUpdate;
    @Column(name = "positionid")
    private Integer positionId;
    @Column(name = "groupid")
    private Integer groupId;
    @Column(name = "attributes")
    private String attributes;
    @Column(name = "phone")
    private String phone;
    @Column(name = "model")
    private String model;
    @Column(name = "contact")
    private String contact;
    @Column(name = "category")
    private String category;
    @Column(name = "disabled")
    private Boolean disabled;
    @Column(name = "crt_name")
    private String crtName;
    @Column(name = "bact_name")
    private String bactName;
    @Column(name = "crt")
    private Integer crt;
    @Column(name = "bact")
    private Integer bact;
    @Column(name = "pflds_1")
    private String vin;
    @Column(name = "pflds_2")
    private String number;
}*/
